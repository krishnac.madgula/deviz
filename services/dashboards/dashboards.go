package dashboards

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type dashboardService struct {
	ds stores.DashboardStore
	cs stores.ChartStore
}

func NewDashboardService(ds stores.DashboardStore, cs stores.ChartStore) *dashboardService {
	return &dashboardService{
		ds: ds,
		cs: cs,
	}
}

func (s *dashboardService) Save(d models.Dashboard) error {
	return s.ds.Save(d)
}

func (s *dashboardService) FetchAll(f filters.Dashboard) ([]models.Dashboard, error) {
	return s.ds.List()
}
func (s *dashboardService) FetchDetails(dashboardId int) (models.DashboardDetails, error) {
	dashboard, err := s.ds.GetById(dashboardId)
	if err != nil {
		return models.DashboardDetails{}, err
	}
	charts, err := s.cs.List(filters.Chart{DashboardId: dashboardId})
	if err != nil {
		return models.DashboardDetails{}, err
	}
	return models.DashboardDetails{Charts: charts, Dashboard: dashboard}, nil
}
