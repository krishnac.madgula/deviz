package models

import (
	"github.com/zopnow/null"
)

type ColumnInfo struct {
	ColumnName      string
	ColumnType      string
	DataType        string
	OrdinalPosition null.String
	ColumnDefault   null.String
	IsNullable      null.String
	Extra           null.String
	ColumnKey       null.String
}
type Columns []ColumnInfo
