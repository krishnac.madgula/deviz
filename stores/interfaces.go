package stores

import (
	"gitlab.com/Inferenz/DeViz/filters"
	models "gitlab.com/Inferenz/DeViz/models"
)

type TableInfo interface {
	Get(database string) (models.Tables, error)
}
type ColumnInfo interface {
	Get(database string, tablename string) (models.Columns, error)
}

type DBInfo interface {
	Get() ([]string, error)
}

type DatasourceStore interface {
	Save(ds models.Datasource) error
	List(f filters.Datasource) ([]models.Datasource, error)
}

type RelationshipStore interface {
	Save(r models.Relationship) error
	List(f filters.Relationship) ([]models.Relationship, error)
}

type ChartStore interface {
	Save(chart models.Chart) error
	GetById(id int) (models.Chart, error)
	List(f filters.Chart) ([]models.Chart, error)
}
type DashboardStore interface {
	Save(dash models.Dashboard) error
	GetById(id int) (models.Dashboard, error)
	List() ([]models.Dashboard, error)
}
type ChartSamplerStore interface {
	Get(chart models.Chart, datasources []models.Datasource,
		relations []models.Relationship) (models.ChartSample, error)
	GetWithFilters(chart models.Chart, datasources []models.Datasource,
		relations []models.Relationship, fAgg models.FiltersAndAggregators) (models.ChartSample, error)
}

type FilterAggregatorStore interface {
	Save(d models.FiltersAndAggregators) error
	Get(chartId int) (models.FiltersAndAggregators, error)
}
