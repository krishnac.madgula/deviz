package migrations

import (
	"fmt"

	"gitlab.com/zopsmart/backend/zs"
)

var zs20191007163100 = &zs.Migration{
	Up: func(m *zs.Migration) error {
		fmt.Println("Running migration up: 20191007163100_addTablenameColumnDatasource.go")
		var err error
		_, err = m.DB.Exec(`ALTER TABLE datasources ADD COLUMN tablename varchar(100)`)
		if err != nil {
			return err
		}

		return nil
	},

	Down: func(m *zs.Migration) error {
		fmt.Println("Running migration down: 20191007163100_addTablenameColumnDatasource.go")

		return nil
	},
}