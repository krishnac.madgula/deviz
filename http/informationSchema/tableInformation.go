package informationSchema

import (
	"net/http"

	zsTypes "gitlab.com/zopsmart/backend/zs/types"

	"gitlab.com/Inferenz/DeViz/services"
)

type tableInfo struct {
	tableService services.TableInfo
}

func NewTableHandler(tableService services.TableInfo) *tableInfo {
	return &tableInfo{tableService: tableService}
}

func (d *tableInfo) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	database := q.Get("database")
	if database == "" {
		return nil, zsTypes.ErrInvalidParam{Param: []string{"database"}}
	}
	return d.tableService.GetTables(database)
}
