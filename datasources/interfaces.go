package datasources

type Datasource interface {
	Connect()
}
