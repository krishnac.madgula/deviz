package charts

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type chartService struct {
	cs stores.ChartStore
	rs stores.RelationshipStore
}

func NewChartService(cs stores.ChartStore, rs stores.RelationshipStore) *chartService {
	return &chartService{
		cs: cs,
		rs: rs,
	}
}

func (s *chartService) Save(c models.Chart) error {
	return s.cs.Save(c)
}

func (s *chartService) FetchAll(f filters.Chart) ([]models.Chart, error) {
	return s.cs.List(f)
}
func (s *chartService) FetchDetails(chartId int) (models.ChartDetails, error) {
	chart, err := s.cs.GetById(chartId)
	if err != nil {
		return models.ChartDetails{}, err
	}
	rels, err := s.rs.List(filters.Relationship{ChartId: chartId})
	if err != nil {
		return models.ChartDetails{}, err
	}
	return models.ChartDetails{Relationships: rels, Chart: chart}, nil
}
