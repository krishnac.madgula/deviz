package datasources

import "database/sql"

//here we provide the interfaces to mysql connectons.
type ConfigSet struct {
	Username     string
	Password     string
	Host         string
	Port         string
	Database     string
	DatabaseType string
}
type MySQLDatasource struct {
	Config    ConfigSet
	Tablename string
}

func FetchDBs() []string {
	//function to get all available databases
	return []string{}
}

func FetchTablesInDB(db *sql.DB) []string {
	//function to fetch all tables in databases
	return []string{}
}

func FetchAllColumnsInTable(db *sql.DB, tablename string) []string {
	//functions to fetch all columns in the table
	return []string{}
}
