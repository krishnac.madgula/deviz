package relationships

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"database/sql"
	"encoding/json"

	models "gitlab.com/Inferenz/DeViz/models"
)

type relationshipStore struct {
	db *sql.DB
}

func NewRelationshipStore(db *sql.DB) *relationshipStore {
	return &relationshipStore{db: db}
}

func (s *relationshipStore) Save(r models.Relationship) error {
	query := "insert into relationships(chart_id, datasource_1_id, datasource_2_id, relationship_type, columns, conditions) values (?,?,?,?,?,?)"
	col, _ := json.Marshal(r.Columns)
	if _, err := s.db.Exec(query, r.ChartId, r.Datasource1Id, r.Datasource2Id, r.RelationshipType, string(col), r.Conditions); err != nil {
		return err
	}
	return nil
}

func (s *relationshipStore) List(f filters.Relationship) ([]models.Relationship, error) {
	result := make([]models.Relationship, 0)
	query := "select id, chart_id, datasource_1_id, datasource_2_id, relationship_type, columns, conditions from relationships where chart_id = ?"
	rows, err := s.db.Query(query, f.ChartId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {

		cols := []string{}
		var id, cid, d1, d2 int
		var relType, columns, condition string
		if err := rows.Scan(&id, &cid, &d1, &d2, &relType, &columns, &condition); err != nil {
			return nil, err
		}
		if err := json.Unmarshal([]byte(columns), &cols); err != nil {
			return nil, err
		}
		temp := models.Relationship{Id: id, ChartId: cid, Datasource1Id: d1, Datasource2Id: d2, RelationshipType: relType, Columns: cols, Conditions: condition}
		result = append(result, temp)
	}
	return result, nil
}
