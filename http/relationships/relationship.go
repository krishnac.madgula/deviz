package relationships

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/Inferenz/DeViz/filters"

	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type relationshipServer struct {
	rs services.RelationshipService
}

func NewRelationshipServer(rs services.RelationshipService) *relationshipServer {
	return &relationshipServer{rs: rs}
}

func (s *relationshipServer) Create(r *http.Request) (interface{}, error) {
	var rel models.Relationship
	req, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(req, &rel); err != nil {
		return nil, err
	}
	if rel.ChartId == 0 {
		return nil, zsTypes.ErrMissingParam{Param: "chartId"}
	}
	if rel.Conditions == "" {
		return nil, zsTypes.ErrMissingParam{Param: "conditions"}
	}
	if rel.Columns == nil {
		return nil, zsTypes.ErrMissingParam{Param: "columns"}
	}
	if rel.Datasource1Id == 0 || rel.Datasource2Id == 0 {
		return nil, zsTypes.Error{Code: http.StatusBadRequest, Message: "Datasources Not Selected"}
	}
	if err := s.rs.Save(rel); err != nil {
		return nil, err
	}
	return http.StatusCreated, nil
}

func (s *relationshipServer) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	cid := q.Get("chartId")
	if cid == "" {
		return nil, zsTypes.ErrMissingParam{Param: "chartId"}
	}
	chartId, _ := strconv.Atoi(cid)
	return s.rs.FetchAll(filters.Relationship{ChartId: chartId})
}
