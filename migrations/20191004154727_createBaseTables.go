package migrations

import (
	"fmt"

	"gitlab.com/zopsmart/backend/zs"
)

var zs20191004154727 = &zs.Migration{
	Up: func(m *zs.Migration) error {
		fmt.Println("Running migration up: 20191004154727_createBaseTables.go")
		var err error
		_, err = m.DB.Exec(`CREATE TABLE dashboards(id int NOT NULL auto_increment, name varchar(50), PRIMARY KEY (id))`)
		if err != nil {
			return err
		}
		_, err = m.DB.Exec(`CREATE TABLE datasources(id int NOT NULL auto_increment, dashboard_id int, config varchar(500), PRIMARY KEY (id), CONSTRAINT datasource_dashboard_fk FOREIGN KEY (dashboard_id) REFERENCES dashboards(id))`)
		if err != nil {
			return err
		}
		_, err = m.DB.Exec(`CREATE TABLE charts(id int NOT NULL auto_increment, dashboard_id int, name varchar(100), type varchar(20), PRIMARY KEY (id), CONSTRAINT chart_dashboard_fk FOREIGN KEY (dashboard_id) REFERENCES dashboards(id))`)
		if err != nil {
			return err
		}
		_, err = m.DB.Exec(`CREATE TABLE relationships
			(id int NOT NULL auto_increment, 
			chart_id int, datasource_1_id int, datasource_2_id int,relationship_type varchar(50), columns varchar(500), 
			conditions varchar(500), 
			PRIMARY KEY (id), 
			CONSTRAINT rel_chart_fk FOREIGN KEY (chart_id) REFERENCES charts(id), 
			CONSTRAINT rel_ds1_fk FOREIGN KEY (datasource_1_id) REFERENCES datasources(id), 
			CONSTRAINT rel_ds2_fk FOREIGN KEY (datasource_2_id) REFERENCES datasources(id)) `)
		if err != nil {
			return err
		}
		return nil
	},

	Down: func(m *zs.Migration) error {
		fmt.Println("Running migration down: 20191004154727_createBaseTables.go")

		return nil
	},
}
