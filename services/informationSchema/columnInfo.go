package informationSchema

import (
	models "gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type columnInfo struct {
	infoStore stores.ColumnInfo
}

func NewColumnService(infoStore stores.ColumnInfo) *columnInfo {
	return &columnInfo{infoStore: infoStore}
}

func (s *columnInfo) GetColumns(database string, tablename string) (models.Columns, error) {
	return s.infoStore.Get(database, tablename)
}
