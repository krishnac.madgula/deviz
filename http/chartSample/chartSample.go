package chartSample

import (
	"net/http"
	"strconv"

	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type chartSample struct {
	cs services.ChartSample
}

func NewChartSample(cs services.ChartSample) *chartSample {
	return &chartSample{cs: cs}
}

func (s *chartSample) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	cid := q.Get("chartId")
	if cid == "" {
		return nil, zsTypes.ErrMissingParam{Param: "chartId"}
	}
	withFilters := q.Get("withFilters")
	chartId, _ := strconv.Atoi(cid)
	if withFilters != "true" {
		return s.cs.GenerateSample(chartId)
	}
	return s.cs.GenerateChartData(chartId)
}
