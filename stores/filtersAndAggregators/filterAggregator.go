package filtersAndAggregators

import (
	"database/sql"
	"encoding/json"

	"gitlab.com/Inferenz/DeViz/models"
)

type filterAgg struct {
	db *sql.DB
}

func NewFilterAggregatorStore(db *sql.DB) *filterAgg {
	return &filterAgg{db: db}
}

func (s *filterAgg) Save(d models.FiltersAndAggregators) error {
	query := "insert into filters_and_aggregators(chart_id, group_by, aggregator, filter) values(?,?,?,?)"
	group_by, _ := json.Marshal(d.GroupBy)
	aggregator, _ := json.Marshal(d.Aggregator)
	if _, err := s.db.Exec(query, d.ChartId, string(group_by), string(aggregator), d.Filter); err != nil {
		return err
	}
	return nil
}

func (s *filterAgg) Get(chartId int) (models.FiltersAndAggregators, error) {
	query := "select id, chart_id, group_by, aggregator, filter from filters_and_aggregators where chart_id=?"
	row := s.db.QueryRow(query, chartId)
	var id, cId int
	var groupBy, agg, filter string
	err := row.Scan(&id, &cId, &groupBy, &agg, &filter)
	if err != nil {
		return models.FiltersAndAggregators{}, err
	}
	gb := []string{}
	_ = json.Unmarshal([]byte(groupBy), &gb)
	aggregator := []string{}
	_ = json.Unmarshal([]byte(agg), &aggregator)
	return models.FiltersAndAggregators{
		ChartId:    cId,
		Id:         id,
		Aggregator: aggregator,
		GroupBy:    gb,
		Filter:     filter,
	}, nil
}
