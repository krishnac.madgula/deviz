package relationships

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type relationshipService struct {
	rs stores.RelationshipStore
}

func NewRelationshipStore(rs stores.RelationshipStore) *relationshipService {
	return &relationshipService{rs: rs}
}

func (s *relationshipService) Save(r models.Relationship) error {
	return s.rs.Save(r)
}
func (s *relationshipService) FetchAll(f filters.Relationship) ([]models.Relationship, error) {
	return s.rs.List(f)
}
