package informationSchema

import (
	"gitlab.com/Inferenz/DeViz/stores"
)

type dbInfo struct {
	infoStore stores.DBInfo
}

func NewDBInfoService(infoStore stores.DBInfo) *dbInfo {
	return &dbInfo{infoStore: infoStore}
}

func (s *dbInfo) GetDBs() ([]string, error) {
	return s.infoStore.Get()
}
