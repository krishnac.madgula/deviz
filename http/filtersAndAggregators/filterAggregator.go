package filtersAndAggregators

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type filterAggregatorServer struct {
	fs services.FilterAggregatorService
}

func NewFilterAggregatorServer(fs services.FilterAggregatorService) *filterAggregatorServer {
	return &filterAggregatorServer{fs: fs}
}

func (s *filterAggregatorServer) Create(r *http.Request) (interface{}, error) {
	model := models.FiltersAndAggregators{}
	req, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(req, &model); err != nil {
		return nil, err
	}
	if model.ChartId == 0 {
		return nil, zsTypes.ErrMissingParam{Param: "chartId"}
	}
	if model.GroupBy == nil {
		return nil, zsTypes.ErrMissingParam{Param: "groupBy"}
	}
	if model.Aggregator == nil {
		return nil, zsTypes.ErrMissingParam{Param: "aggregator"}
	}
	if err := s.fs.Save(model); err != nil {
		return nil, err
	}
	return nil, nil
}

func (s *filterAggregatorServer) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	cid := q.Get("chartId")
	if cid == "" {
		return nil, zsTypes.ErrMissingParam{Param: "chartId"}
	}
	chartId, _ := strconv.Atoi(cid)
	return s.fs.Fetch(chartId)
}
