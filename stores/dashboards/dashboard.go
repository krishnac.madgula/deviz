package dashboards

import (
	"database/sql"
	"fmt"

	models "gitlab.com/Inferenz/DeViz/models"
)

type dashboardStore struct {
	db *sql.DB
}

func NewDashboardStore(db *sql.DB) *dashboardStore {
	return &dashboardStore{db: db}
}

func (s *dashboardStore) Save(dash models.Dashboard) error {
	fmt.Println(dash.Name)
	query := "insert into dashboards (name) values (?)"
	if _, err := s.db.Exec(query, dash.Name); err != nil {
		return err
	}
	return nil
}

func (s *dashboardStore) GetById(id int) (models.Dashboard, error) {
	query := "select id, name from dashboards where id = ?"
	row := s.db.QueryRow(query, id)
	var did int
	var dname string
	row.Scan(&did, &dname)
	return models.Dashboard{Id: did, Name: dname}, nil
}

func (s *dashboardStore) List() ([]models.Dashboard, error) {
	query := "select id, name from dashboards"
	result := make([]models.Dashboard, 0)
	rows, err := s.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var dId int
		var dname string
		err := rows.Scan(&dId, &dname)
		if err != nil {
			return nil, err
		}
		temp := models.Dashboard{Id: dId, Name: dname}
		result = append(result, temp)
	}
	return result, nil

}
