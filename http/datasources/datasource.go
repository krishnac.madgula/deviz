package datasources

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/Inferenz/DeViz/filters"

	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type datasourceServer struct {
	dService services.DatasourceService
}

func NewDatasourceServer(dService services.DatasourceService) *datasourceServer {
	return &datasourceServer{dService: dService}
}

func (s *datasourceServer) Create(r *http.Request) (interface{}, error) {
	var ds models.Datasource
	req, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(req, &ds); err != nil {
		return nil, err
	}
	if ds.DashboardId == 0 {
		return nil, zsTypes.ErrMissingParam{Param: "dashboardId"}
	}
	//validate configuration here
	if err := s.dService.Save(ds); err != nil {
		return nil, err
	}
	return http.StatusCreated, nil

}
func (s *datasourceServer) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	ds := q.Get("dashboardId")
	//validate filters
	if ds == "" {
		return nil, zsTypes.ErrMissingParam{Param: "dashboardId"}
	}
	did, _ := strconv.Atoi(ds)
	return s.dService.FetchAll(filters.Datasource{DashboardId: did})
}
