package informationSchema

import (
	"database/sql"

	models "gitlab.com/Inferenz/DeViz/models"
)

type tableInformation struct {
	db *sql.DB
}

func NewTableStore(db *sql.DB) *tableInformation {
	return &tableInformation{
		db: db,
	}
}

func (s *tableInformation) Get(database string) (models.Tables, error) {
	result := models.Tables{}
	query := "select table_name, table_schema from information_schema.tables where table_schema = ?"
	rows, err := s.db.Query(query, database)
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		temp := models.TableInfo{}
		rows.Scan(&temp.TableName, &temp.Database)
		result = append(result, temp)
	}
	return result, nil
}
