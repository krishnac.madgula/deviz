package informationSchema

import "database/sql"

type dbInfo struct {
	db *sql.DB
}

func NewDBInfoStore(db *sql.DB) *dbInfo {
	return &dbInfo{db: db}
}

func (s *dbInfo) Get() ([]string, error) {
	result := []string{}
	query := "select schema_name from information_schema.schemata"
	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		temp := ""
		err := rows.Scan(&temp)
		if err != nil {
			return nil, err
		}
		result = append(result, temp)
	}
	return result, nil
}
