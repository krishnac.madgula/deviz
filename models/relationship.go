package models

//Basic relationship structure temporary
//RelationshipType to be an enum
type Relationship struct {
	Id               int
	ChartId          int
	Datasource1Id    int
	Datasource2Id    int
	RelationshipType string
	Columns          []string
	Conditions       string
}
