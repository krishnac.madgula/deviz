package informationSchema

import (
	models "gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type tableInfo struct {
	infoStore stores.TableInfo
}

func NewTableService(infoStore stores.TableInfo) *tableInfo {
	return &tableInfo{infoStore: infoStore}
}

func (s *tableInfo) GetTables(database string) (models.Tables, error) {
	return s.infoStore.Get(database)
}
