package informationSchema

import (
	"database/sql"
	"log"

	models "gitlab.com/Inferenz/DeViz/models"
)

type columnInformation struct {
	db *sql.DB
}

func NewColumnStore(db *sql.DB) *columnInformation {
	return &columnInformation{
		db: db,
	}
}

func (s *columnInformation) Get(database string, tablename string) (models.Columns, error) {
	result := models.Columns{}
	query := "select column_name, ordinal_position, column_default, is_nullable, data_type, column_key," +
		"extra, column_type from information_schema.columns where table_schema = ? and table_name = ?"
	rows, err := s.db.Query(query, database, tablename)
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		temp := models.ColumnInfo{}
		err := rows.Scan(&temp.ColumnName, &temp.OrdinalPosition, &temp.ColumnDefault,
			&temp.IsNullable, &temp.DataType, &temp.ColumnKey, &temp.Extra, &temp.ColumnType)
		if err != nil {
			log.Println(err)
			return nil, err
		}
		result = append(result, temp)
	}
	return result, nil
}
