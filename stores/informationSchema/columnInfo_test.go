package informationSchema

import (
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"testing"

	models "gitlab.com/Inferenz/DeViz/models/informationSchema"
	"gitlab.com/zopsmart/backend/zs"
)

func TestColumnGet(t *testing.T) {
	testCase := struct {
		expected models.Columns
		err      error
		inputDB    string
		inputTable string
	}{
		expected: models.Columns{
			models.ColumnInfo{
				ColumnName: "id",
				ColumnType: "int",
			},
		},
		inputDB: "order_service",
		inputTable: "orders",
		err:   nil,
	}
	logger := log.New(ioutil.Discard, "[test]", log.LstdFlags)
	db := zs.NewMYSQL(logger, zs.MySQLConfig{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Database: "information_schema",
		Port:     "3306",
		HostName: os.Getenv("DB_HOST"),
	})
	ts := NewColumnStore(db)
	
	exp, err := ts.Get(testCase.inputDB, testCase.inputTable)
	log.Println(err)
	if !reflect.DeepEqual(exp, testCase.expected) {
		log.Println(exp)
	}

}
