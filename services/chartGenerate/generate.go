package chartGenerate

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"

	"gitlab.com/Inferenz/DeViz/stores"
)

type chartSampler struct {
	cs  stores.ChartStore
	rs  stores.RelationshipStore
	ds  stores.DatasourceStore
	css stores.ChartSamplerStore
	fs  stores.FilterAggregatorStore
}

func NewChartSampler(cs stores.ChartStore, ds stores.DatasourceStore, rs stores.RelationshipStore, css stores.ChartSamplerStore, fs stores.FilterAggregatorStore) *chartSampler {
	return &chartSampler{
		cs:  cs,
		rs:  rs,
		ds:  ds,
		css: css,
		fs:  fs,
	}
}
func (s *chartSampler) GenerateSample(chartId int) (models.ChartSample, error) {
	chart, err := s.cs.GetById(chartId)
	if err != nil {
		return models.ChartSample{}, err
	}
	datasources, err := s.ds.List(filters.Datasource{DashboardId: chart.DashboardId})
	if err != nil {
		return models.ChartSample{}, err
	}
	relations, err := s.rs.List(filters.Relationship{ChartId: chartId})
	return s.css.Get(chart, datasources, relations)
}
func (s *chartSampler) GenerateChartData(chartId int) (models.ChartSample, error) {
	chart, err := s.cs.GetById(chartId)
	if err != nil {
		return models.ChartSample{}, err
	}
	datasources, err := s.ds.List(filters.Datasource{DashboardId: chart.DashboardId})
	if err != nil {
		return models.ChartSample{}, err
	}
	relations, err := s.rs.List(filters.Relationship{ChartId: chartId})
	if err != nil {
		return models.ChartSample{}, err
	}
	fAgg, err := s.fs.Get(chartId)
	if err != nil {
		return models.ChartSample{}, err
	}
	return s.css.GetWithFilters(chart, datasources, relations, fAgg)
}
