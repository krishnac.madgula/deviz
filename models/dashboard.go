package models

//temporary dashboard model
type Dashboard struct {
	Id   int
	Name string
}
type DashboardDetails struct {
	Dashboard
	Charts []Chart
}
