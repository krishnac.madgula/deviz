package datasources

import (
	"database/sql"
	"encoding/json"

	"github.com/zopnow/null"

	"gitlab.com/Inferenz/DeViz/filters"

	models "gitlab.com/Inferenz/DeViz/models"
)

type datasourceStore struct {
	db *sql.DB
}

func NewDatasourceStore(db *sql.DB) *datasourceStore {
	return &datasourceStore{db: db}
}

func (s *datasourceStore) Save(ds models.Datasource) error {
	config, err := json.Marshal(ds.Config)
	if err != nil {
		return err
	}
	query := "insert into datasources (dashboard_id, tablename, config) values (?,?,?)"
	if _, err = s.db.Exec(query, ds.DashboardId, ds.Tablename, string(config)); err != nil {
		return err
	}
	return nil
}

func (s *datasourceStore) List(f filters.Datasource) ([]models.Datasource, error) {
	result := make([]models.Datasource, 0)
	query := "select id, dashboard_id, tablename, config from datasources where dashboard_id = ?"
	rows, err := s.db.Query(query, f.DashboardId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {

		var id, dashboardId int
		var conf string
		var tablename null.String
		err = rows.Scan(&id, &dashboardId, &tablename, &conf)
		if err != nil {
			return nil, err
		}
		var temp models.ConfigSet
		err = json.Unmarshal([]byte(conf), &temp)
		if err != nil {
			return nil, err
		}
		cur := models.Datasource{DashboardId: dashboardId, Id: id, Tablename: tablename, Config: temp}
		result = append(result, cur)
	}
	return result, nil
}
