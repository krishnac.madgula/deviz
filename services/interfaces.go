package services

import (
	"gitlab.com/Inferenz/DeViz/filters"
	models "gitlab.com/Inferenz/DeViz/models"
)

type TableInfo interface {
	GetTables(database string) (models.Tables, error)
}
type ColumnInfo interface {
	GetColumns(database string, tablename string) (models.Columns, error)
}

type DBInfo interface {
	GetDBs() ([]string, error)
}

type DatasourceService interface {
	FetchAll(f filters.Datasource) ([]models.Datasource, error)
	Save(d models.Datasource) error
}
type RelationshipService interface {
	FetchAll(f filters.Relationship) ([]models.Relationship, error)
	Save(r models.Relationship) error
}

type ChartService interface {
	Save(c models.Chart) error
	FetchAll(f filters.Chart) ([]models.Chart, error)
	FetchDetails(chartId int) (models.ChartDetails, error)
}
type DashboardService interface {
	Save(d models.Dashboard) error
	FetchAll(f filters.Dashboard) ([]models.Dashboard, error)
	FetchDetails(dashboardId int) (models.DashboardDetails, error)
}
type ChartSample interface {
	GenerateSample(chartId int) (models.ChartSample, error)
	GenerateChartData(chartId int) (models.ChartSample, error)
}
type FilterAggregatorService interface {
	Save(d models.FiltersAndAggregators) error
	Fetch(chartId int) (models.FiltersAndAggregators, error)
}
