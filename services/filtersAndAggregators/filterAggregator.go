package filtersAndAggregators

import (
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type filterAggregator struct {
	fs stores.FilterAggregatorStore
}

func NewFilterAggregatorService(fs stores.FilterAggregatorStore) *filterAggregator {
	return &filterAggregator{fs: fs}
}

func (s *filterAggregator) Save(d models.FiltersAndAggregators) error {
	return s.fs.Save(d)
}

func (s *filterAggregator) Fetch(chartId int) (models.FiltersAndAggregators, error) {
	return s.fs.Get(chartId)
}
