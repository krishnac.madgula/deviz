package dashboards

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"path"
	"strconv"

	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type dashboardServer struct {
	ds services.DashboardService
}

func NewDashboardServer(ds services.DashboardService) *dashboardServer {
	return &dashboardServer{ds: ds}
}

func (s *dashboardServer) Create(r *http.Request) (interface{}, error) {
	var d models.Dashboard
	req, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(req, &d); err != nil {
		return nil, err
	}
	if d.Name == "" {
		return nil, zsTypes.ErrMissingParam{Param: "name"}
	}
	if err := s.ds.Save(d); err != nil {
		return nil, err
	}
	return http.StatusCreated, nil
}

func (s *dashboardServer) Read(r *http.Request) (interface{}, error) {
	did := path.Base(r.URL.Path)
	dashboardId, _ := strconv.Atoi(did)
	if dashboardId == 0 {
		return nil, zsTypes.ErrInvalidParam{Param: []string{"id"}}
	}
	return s.ds.FetchDetails(dashboardId)
}

func (s *dashboardServer) Index(r *http.Request) (interface{}, error) {
	// q := r.URL.Query()
	//TODO dashboard Filters
	return s.ds.FetchAll(filters.Dashboard{})
}
