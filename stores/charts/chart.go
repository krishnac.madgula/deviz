package charts

import (
	"database/sql"

	"gitlab.com/Inferenz/DeViz/filters"
	models "gitlab.com/Inferenz/DeViz/models"
)

type chartStore struct {
	db *sql.DB
}

func NewChartStore(db *sql.DB) *chartStore {
	return &chartStore{db: db}
}

func (s *chartStore) Save(chart models.Chart) error {
	query := "insert into charts(type, name, dashboard_id) values (?,?,?)"
	if _, err := s.db.Exec(query, chart.Type, chart.Name, chart.DashboardId); err != nil {
		return err
	}
	return nil
}

func (s *chartStore) GetById(id int) (models.Chart, error) {
	query := "select id, dashboard_id, name, type from charts where id = ?"
	row := s.db.QueryRow(query, id)
	var cid, dId int
	var ctype, cname string
	row.Scan(&cid, &dId, &cname, &ctype)
	return models.Chart{Id: cid, Name: cname, Type: ctype, DashboardId: dId}, nil
}

func (s *chartStore) List(f filters.Chart) ([]models.Chart, error) {
	query := "select id, dashboard_id, name, type from charts where dashboard_id = ?"
	result := make([]models.Chart, 0)
	rows, err := s.db.Query(query, f.DashboardId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var cid, dId int
		var ctype, cname string
		err := rows.Scan(&cid, &dId, &cname, &ctype)
		if err != nil {
			return nil, err
		}
		temp := models.Chart{Id: cid, Name: cname, Type: ctype, DashboardId: dId}
		result = append(result, temp)
	}
	return result, nil

}
