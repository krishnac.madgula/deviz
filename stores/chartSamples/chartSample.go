package chartSamples

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"gitlab.com/Inferenz/DeViz/models"
)

type chartSamplerStore struct {
	db *sql.DB
}

func NewChartSamplerStore(db *sql.DB) *chartSamplerStore {
	return &chartSamplerStore{db: db}
}

func (s *chartSamplerStore) Get(chart models.Chart, datasources []models.Datasource,
	relations []models.Relationship) (models.ChartSample, error) {
	dsmap := getDatasourceDetails(datasources)
	baseDId := findBaseTable(relations)
	if baseDId == 0 {
		return models.ChartSample{}, errors.New("incorrect relationships defined")
	}
	relMap := structureRelations(relations)

	query, headers := buildQueryAndHeaders(relMap, dsmap, baseDId)

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println(err)
		return models.ChartSample{}, err
	}
	log.Println(query)
	resultRows := make([][]string, 0)
	rowValues := make([]interface{}, len(headers))
	rowPtr := make([]interface{}, len(headers))
	for i := range rowPtr {
		rowValues[i] = &rowPtr[i]
	}
	for rows.Next() {
		err := rows.Scan(rowValues...)
		if err != nil {
			log.Println(err)
			return models.ChartSample{}, err
		}
		curRow := make([]string, len(headers))
		for i := range rowValues {
			x, _ := rowValues[i].(*interface{})
			b, _ := (*x).([]byte)
			curRow[i] = fmt.Sprintf("%v", string(b))
		}
		resultRows = append(resultRows, curRow)
	}
	return models.ChartSample{Headers: headers, Rows: resultRows}, nil

}
func (s *chartSamplerStore) GetWithFilters(chart models.Chart, datasources []models.Datasource,
	relations []models.Relationship, fAgg models.FiltersAndAggregators) (models.ChartSample, error) {
	dsmap := getDatasourceDetails(datasources)
	baseDId := findBaseTable(relations)
	if baseDId == 0 {
		return models.ChartSample{}, errors.New("incorrect relationships defined")
	}
	relMap := structureRelations(relations)

	query, headers := buildQueryAndHeadersWithFilters(relMap, dsmap, baseDId, fAgg)

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println(err)
		return models.ChartSample{}, err
	}
	log.Println(query)
	resultRows := make([][]string, 0)
	rowValues := make([]interface{}, len(headers))
	rowPtr := make([]interface{}, len(headers))
	for i := range rowPtr {
		rowValues[i] = &rowPtr[i]
	}
	for rows.Next() {
		err := rows.Scan(rowValues...)
		if err != nil {
			log.Println(err)
			return models.ChartSample{}, err
		}
		curRow := make([]string, len(headers))
		for i := range rowValues {
			x, _ := rowValues[i].(*interface{})
			b, _ := (*x).([]byte)
			curRow[i] = fmt.Sprintf("%v", string(b))
		}
		resultRows = append(resultRows, curRow)
	}
	return models.ChartSample{Headers: headers, Rows: resultRows}, nil

}

func getDatasourceDetails(ds []models.Datasource) map[int]string {
	res := make(map[int]string, 0)
	for i := range ds {
		res[ds[i].Id] = fmt.Sprintf("%s.%s", ds[i].Config.Database, ds[i].Tablename.String)
	}
	return res
}

func buildQueryAndHeaders(relMap map[int][]models.Relationship, dsMap map[int]string, baseDId int) (string, []string) {

	columns := []string{}
	condition := []string{}
	joins := []string{}
	d2 := []string{}
	ds := make([]int, 0)
	for k, _ := range relMap {
		if k != baseDId {
			ds = append(ds, k)
		}
	}
	//first go through the base table mappings
	for _, v := range relMap[baseDId] {
		columns = append(columns, v.Columns...)
		condition = append(condition, v.Conditions)
		joins = append(joins, v.RelationshipType)
		// d1 = append(d1, dsMap[v.Datasource1Id])
		d2 = append(d2, dsMap[v.Datasource2Id])
	}
	for i := range ds {
		for _, v := range relMap[ds[i]] {
			columns = append(columns, v.Columns...)
			condition = append(condition, v.Conditions)
			joins = append(joins, v.RelationshipType)
			// d1 = append(d1, dsMap[v.Datasource1Id])
			d2 = append(d2, dsMap[v.Datasource2Id])
		}
	}
	uniqueDs := make(map[int]bool, 0)
	query := "select "
	for i := range columns {
		query = query + columns[i]
		if i != len(columns)-1 {
			query += ","
		}
	}
	query += " from"
	query += " " + dsMap[baseDId]
	uniqueDs[baseDId] = true
	for i := range joins {

		query += " " + joins[i]
		query += " " + d2[i]
		query += " " + " ON "
		query += " " + condition[i]

	}

	return query, columns
}
func buildQueryAndHeadersWithFilters(relMap map[int][]models.Relationship,
	dsMap map[int]string, baseDId int, fAgg models.FiltersAndAggregators) (string, []string) {

	columns := []string{}
	condition := []string{}
	joins := []string{}
	d2 := []string{}
	ds := make([]int, 0)
	for k, _ := range relMap {
		if k != baseDId {
			ds = append(ds, k)
		}
	}
	//first go through the base table mappings
	for _, v := range relMap[baseDId] {
		condition = append(condition, v.Conditions)
		joins = append(joins, v.RelationshipType)
		// d1 = append(d1, dsMap[v.Datasource1Id])
		d2 = append(d2, dsMap[v.Datasource2Id])
	}
	for i := range ds {
		for _, v := range relMap[ds[i]] {
			condition = append(condition, v.Conditions)
			joins = append(joins, v.RelationshipType)
			// d1 = append(d1, dsMap[v.Datasource1Id])
			d2 = append(d2, dsMap[v.Datasource2Id])
		}
	}
	for i := range fAgg.Aggregator {
		columns = append(columns, fAgg.Aggregator[i])
	}
	for i := range fAgg.GroupBy {
		columns = append(columns, fAgg.GroupBy[i])
	}
	uniqueDs := make(map[int]bool, 0)
	query := "select "
	for i := range columns {
		query = query + columns[i]
		if i != len(columns)-1 {
			query += ","
		}
	}
	query += " from"
	query += " " + dsMap[baseDId]
	uniqueDs[baseDId] = true
	for i := range joins {

		query += " " + joins[i]
		query += " " + d2[i]
		query += " " + " ON "
		query += " " + condition[i]

	}
	if fAgg.Filter != "" {
		query += " where "
		query += fAgg.Filter
	}
	query += " group by "
	for i := range fAgg.GroupBy {
		query = query + fAgg.GroupBy[i]
		if i != len(fAgg.GroupBy)-1 {
			query += ", "
		}
	}

	return query, columns
}
func structureRelations(relations []models.Relationship) map[int][]models.Relationship {
	result := make(map[int][]models.Relationship, 0)
	for i := range relations {
		if _, ok := result[relations[i].Datasource1Id]; !ok {
			result[relations[i].Datasource1Id] = make([]models.Relationship, 0)
		}
		result[relations[i].Datasource1Id] = append(result[relations[i].Datasource1Id], relations[i])
	}
	return result
}
func findBaseTable(relations []models.Relationship) int {
	d1m := make(map[int]bool, 0)
	d1s := []int{}
	d2s := []int{}
	d2m := make(map[int]bool, 0)
	for i := range relations {
		if _, ok := d1m[relations[i].Datasource1Id]; !ok {
			d1m[relations[i].Datasource1Id] = true
			d1s = append(d1s, relations[i].Datasource1Id)
		}
		if _, ok := d2m[relations[i].Datasource2Id]; !ok {
			d2m[relations[i].Datasource2Id] = true
			d2s = append(d2s, relations[i].Datasource2Id)
		}
	}
	for k, _ := range d1m {
		if _, ok := d2m[k]; !ok {
			return k
		}
	}
	return 0
}
