package main

import (
	"log"
	"os"

	chartSampleDelivery "gitlab.com/Inferenz/DeViz/http/chartSample"
	chartDelivery "gitlab.com/Inferenz/DeViz/http/charts"
	dashboardDelivery "gitlab.com/Inferenz/DeViz/http/dashboards"
	datasourceDelivery "gitlab.com/Inferenz/DeViz/http/datasources"
	filterAggDelivery "gitlab.com/Inferenz/DeViz/http/filtersAndAggregators"
	informationDelivery "gitlab.com/Inferenz/DeViz/http/informationSchema"
	relationshipDelivery "gitlab.com/Inferenz/DeViz/http/relationships"
	"gitlab.com/Inferenz/DeViz/migrations"
	chartSampleService "gitlab.com/Inferenz/DeViz/services/chartGenerate"
	chartService "gitlab.com/Inferenz/DeViz/services/charts"
	dashboardService "gitlab.com/Inferenz/DeViz/services/dashboards"
	datasourceService "gitlab.com/Inferenz/DeViz/services/datasources"
	filterAggService "gitlab.com/Inferenz/DeViz/services/filtersAndAggregators"
	informationService "gitlab.com/Inferenz/DeViz/services/informationSchema"
	relationshipServices "gitlab.com/Inferenz/DeViz/services/relationships"
	chartSampleStore "gitlab.com/Inferenz/DeViz/stores/chartSamples"
	chartStore "gitlab.com/Inferenz/DeViz/stores/charts"
	dashboardStore "gitlab.com/Inferenz/DeViz/stores/dashboards"
	datasourceStore "gitlab.com/Inferenz/DeViz/stores/datasources"
	filterAggStore "gitlab.com/Inferenz/DeViz/stores/filtersAndAggregators"
	informationStore "gitlab.com/Inferenz/DeViz/stores/informationSchema"
	relationshipStore "gitlab.com/Inferenz/DeViz/stores/relationships"
	"gitlab.com/zopsmart/backend/zs"
)

func main() {
	infoLog := log.New(os.Stdout, "[INFORMATION_SCHEMA]", 0)
	initLog := log.New(os.Stdout, "[INIT_LOG]", 0)
	informationSchemaDB := zs.NewMYSQL(infoLog, zs.MySQLConfig{Port: "3306", HostName: os.Getenv("DB_HOST"), User: os.Getenv("DB_USER"), Password: os.Getenv("DB_PASSWORD"), Database: "information_schema"})
	deVizDB := zs.NewMYSQL(initLog, zs.MySQLConfig{Database: "deviz", Port: "3306", HostName: os.Getenv("DB_HOST"), User: os.Getenv("DB_USER"), Password: os.Getenv("DB_PASSWORD")})
	mDB := zs.NewMYSQL(initLog, zs.MySQLConfig{Database: "deviz", Port: "3306", HostName: os.Getenv("DB_HOST"), User: os.Getenv("DB_USER"), Password: os.Getenv("DB_PASSWORD")})
	redis := zs.NewRedis(initLog, zs.RedisConfig{HostName: os.Getenv("REDIS_HOST"), Port: "6379"})
	err := zs.Migrate("deviz", mDB, redis, migrations.All)
	if err != nil {
		log.Println("Migration failed with error: ", err)
	}
	//Stores
	cStore := chartStore.NewChartStore(deVizDB)
	dsStore := datasourceStore.NewDatasourceStore(deVizDB)
	dbStore := dashboardStore.NewDashboardStore(deVizDB)
	rStore := relationshipStore.NewRelationshipStore(deVizDB)
	tableStore := informationStore.NewTableStore(informationSchemaDB)
	dbInfoStore := informationStore.NewDBInfoStore(informationSchemaDB)
	columnStore := informationStore.NewColumnStore(informationSchemaDB)
	cssStore := chartSampleStore.NewChartSamplerStore(deVizDB)
	fAggStore := filterAggStore.NewFilterAggregatorStore(deVizDB)
	//Services
	dsService := datasourceService.NewDatasourceService(dsStore)
	rService := relationshipServices.NewRelationshipStore(rStore)
	cService := chartService.NewChartService(cStore, rStore)
	dbService := dashboardService.NewDashboardService(dbStore, cStore)
	cSampleService := chartSampleService.NewChartSampler(cStore, dsStore, rStore, cssStore, fAggStore)
	fAggService := filterAggService.NewFilterAggregatorService(fAggStore)
	tableService := informationService.NewTableService(tableStore)
	dbInfoService := informationService.NewDBInfoService(dbInfoStore)
	columnService := informationService.NewColumnService(columnStore)
	tableDelivery := informationDelivery.NewTableHandler(tableService)
	//Servers/Delivery
	dsServer := datasourceDelivery.NewDatasourceServer(dsService)
	rServer := relationshipDelivery.NewRelationshipServer(rService)
	cServer := chartDelivery.NewChartServer(cService)
	cSampleServer := chartSampleDelivery.NewChartSample(cSampleService)
	fAggServer := filterAggDelivery.NewFilterAggregatorServer(fAggService)
	dbServer := dashboardDelivery.NewDashboardServer(dbService)
	dbInfoDelivery := informationDelivery.NewDBInfoHandler(dbInfoService)
	columnDelivery := informationDelivery.NewColumnHandler(columnService)

	router := zs.NewRouter()
	router.REST("information-schema/tableInfo", tableDelivery)
	router.REST("information-schema/columnInfo", columnDelivery)
	router.REST("information-schema/dbInfo", dbInfoDelivery)
	router.REST("dashboard", dbServer)
	router.REST("datasource", dsServer)
	router.REST("chart", cServer)
	router.REST("relationship", rServer)
	router.REST("chart-sample", cSampleServer)
	router.REST("filter-aggregator", fAggServer)
	zs.StartServers(initLog, &zs.HTTPServer{Router: router, Port: 9170})

}
