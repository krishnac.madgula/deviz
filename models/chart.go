package models

type Chart struct {
	Id          int
	Name        string
	Type        string
	DashboardId int
}
type ChartDetails struct {
	Chart
	Relationships []Relationship
}

type ChartSample struct {
	Headers []string
	Rows    [][]string
}

type FiltersAndAggregators struct {
	ChartId    int
	GroupBy    []string
	Aggregator []string
	Filter     string
	Id         int
}
