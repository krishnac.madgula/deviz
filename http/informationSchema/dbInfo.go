package informationSchema

import (
	"net/http"

	"gitlab.com/Inferenz/DeViz/services"
)

type dbInfo struct {
	dbService services.DBInfo
}

func NewDBInfoHandler(dbService services.DBInfo) *dbInfo {
	return &dbInfo{dbService: dbService}
}

func (d *dbInfo) Index(r *http.Request) (interface{}, error) {
	return d.dbService.GetDBs()
}
