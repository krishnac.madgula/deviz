package charts

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"path"
	"strconv"

	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/services"
	zsTypes "gitlab.com/zopsmart/backend/zs/types"
)

type chartServer struct {
	cs services.ChartService
}

func NewChartServer(cs services.ChartService) *chartServer {
	return &chartServer{cs: cs}
}

func (s *chartServer) Create(r *http.Request) (interface{}, error) {
	var ch models.Chart
	req, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(req, &ch); err != nil {
		return nil, err
	}
	if ch.DashboardId == 0 {
		return nil, zsTypes.ErrMissingParam{Param: "dashboardId"}
	}
	if ch.Name == "" {
		return nil, zsTypes.ErrMissingParam{Param: "name"}
	}
	if ch.Type == "" {
		return nil, zsTypes.ErrMissingParam{Param: "type"}
	}
	if err := s.cs.Save(ch); err != nil {
		return nil, err
	}
	return http.StatusCreated, nil
}

func (s *chartServer) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	did := q.Get("dashboardId")
	if did == "" {
		return nil, zsTypes.ErrMissingParam{Param: "dashboardId"}
	}
	dashboardId, _ := strconv.Atoi(did)
	return s.cs.FetchAll(filters.Chart{DashboardId: dashboardId})
}

func (s *chartServer) Read(r *http.Request) (interface{}, error) {
	cid := path.Base(r.URL.Path)
	chartId, _ := strconv.Atoi(cid)
	if chartId == 0 {
		return nil, zsTypes.ErrInvalidParam{Param: []string{"id"}}
	}
	return s.cs.FetchDetails(chartId)

}
