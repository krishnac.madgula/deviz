package informationSchema

import (
	"net/http"

	zsTypes "gitlab.com/zopsmart/backend/zs/types"

	"gitlab.com/Inferenz/DeViz/services"
)

type columnInfo struct {
	columnService services.ColumnInfo
}

func NewColumnHandler(columnService services.ColumnInfo) *columnInfo {
	return &columnInfo{columnService: columnService}
}

func (d *columnInfo) Index(r *http.Request) (interface{}, error) {
	q := r.URL.Query()
	database := q.Get("database")
	tablename := q.Get("tablename")
	if database == "" {
		return nil, zsTypes.ErrInvalidParam{Param: []string{"database"}}
	}
	if tablename == "" {
		return nil, zsTypes.ErrInvalidParam{Param: []string{"tablename"}}
	}
	return d.columnService.GetColumns(database, tablename)
}
