package datasources

import (
	"gitlab.com/Inferenz/DeViz/filters"
	"gitlab.com/Inferenz/DeViz/models"
	"gitlab.com/Inferenz/DeViz/stores"
)

type datasourceService struct {
	datasourceStore stores.DatasourceStore
}

func NewDatasourceService(dstore stores.DatasourceStore) *datasourceService {
	return &datasourceService{datasourceStore: dstore}
}

func (s *datasourceService) FetchAll(f filters.Datasource) ([]models.Datasource, error) {
	return s.datasourceStore.List(f)
}

func (s *datasourceService) Save(d models.Datasource) error {
	return s.datasourceStore.Save(d)
}
