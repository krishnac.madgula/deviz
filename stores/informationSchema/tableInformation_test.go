package informationSchema

import (
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"testing"

	models "gitlab.com/Inferenz/DeViz/models/informationSchema"
	"gitlab.com/zopsmart/backend/zs"
)

func TestGet(t *testing.T) {
	testCase := struct {
		expected models.Tables
		err      error
		input    string
	}{
		expected: models.Tables{
			models.TableInfo{
				TableName: "orders",
				Database:  "order_service",
			},
		},
		input: "order_service",
		err:   nil,
	}
	logger := log.New(ioutil.Discard, "[test]", log.LstdFlags)
	db := zs.NewMYSQL(logger, zs.MySQLConfig{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Database: "information_schema",
		Port:     "3306",
		HostName: os.Getenv("DB_HOST"),
	})
	ts := NewTableStore(db)
	exp, err := ts.Get(testCase.input)
	log.Println(err)
	if !reflect.DeepEqual(exp, testCase.expected) {
		log.Println(exp)
	}

}
