package models

import "github.com/zopnow/null"

//DB Configs to connect to any db
type ConfigSet struct {
	Username     string
	Password     string
	Host         string
	Port         string
	Database     string
	DatabaseType string
}

type Datasource struct {
	Config      ConfigSet
	Id          int
	DashboardId int
	Tablename   null.String
}
