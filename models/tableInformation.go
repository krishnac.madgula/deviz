package models

//To be extended
type Tables []TableInfo
type TableInfo struct {
	TableName string
	Database  string
}
