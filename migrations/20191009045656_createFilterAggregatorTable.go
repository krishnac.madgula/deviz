package migrations

import (
	"fmt"

	"gitlab.com/zopsmart/backend/zs"
)

var zs20191009045656 = &zs.Migration{
	Up: func(m *zs.Migration) error {
		fmt.Println("Running migration up: 20191009045656_createFilterAggregatorTable.go")
		_, err := m.DB.Exec(`CREATE TABLE filters_and_aggregators
			(id int NOT NULL auto_increment, 
			chart_id int, filter varchar(500), aggregator varchar(500), 
			group_by varchar(500), 
			PRIMARY KEY (id), 
			CONSTRAINT fil_agg_chart_fk FOREIGN KEY (chart_id) REFERENCES charts(id)) `)
		if err != nil {
			return err
		}
		return nil
	},

	Down: func(m *zs.Migration) error {
		fmt.Println("Running migration down: 20191009045656_createFilterAggregatorTable.go")

		return nil
	},
}
